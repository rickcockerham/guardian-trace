
require 'net/smtp'
require 'time'
require 'rubygems'
require 'net/http'
require 'open-uri'
require 'json'
require 'base64'

class GuardianUtil
  def self.get_val
    69
  end

  def self.test_gd_email
    subject = "a test"
    body = "test body"
    addresses = ["gabe.archangelic@gmail.com"]


    Net::SMTP.start(
                    'smtp.1and1.com',
                    25,
                    'guardiantrace.com',
                    'alert@guardiantrace.com',
                    'Pioneer1',
                    :plain)  do |smtp|

=begin
    Net::SMTP.start(
                    'smtpout.secureserver.net',
                    3535,
                    'guardianalert.net',
                    'alert@guardianalert.net',
                    'Pioneer1',
                    :plain)  do |smtp|
=end

=begin
      puts "IN*************"
      msg = "Subject:" + subject + "\nContent-Type:text/plain;charset=ISO-8859-1\nContent-Transfer-Encoding:7bit\n\n" + body

      smtp.sendmail("",
                    #msg,
                    "alert@guardianalert.net", * addresses)
=end

      smtp.open_message_stream('Alert@GuardianTrace.com', addresses) do |f|
        f.puts 'From: Alert@GuardianTrace.com' # 'From: alert@guardianalert.net'
        f.puts 'To: ' + addresses.map {|e| [";", e]}.flatten(1).drop(1).join
        f.puts 'Subject: test message 3'
        f.puts ''
        f.puts 'This is a test message.'
      end

    end

  end

  def self.send_email (addresses, subject, body)
    if addresses.any?
      Net::SMTP.start(
                      'smtp.1and1.com',
                      25,
                      'guardiantrace.com',
                      'alert@guardiantrace.com',
                      'Pioneer1',
                      :plain)  do |smtp|
        
        smtp.open_message_stream('Alert@GuardianTrace.com', addresses) do |f|
          f.puts 'From: Alert@GuardianTrace.com'
          f.puts 'To: ' + addresses.map {|e| [";", e]}.flatten(1).drop(1).join
          f.puts 'Subject: ' + subject
          f.puts ''
          f.puts body
        end
      end
    end

=begin
       msg = "Subject: " + subject + "\nDate: #{Time.now.rfc2822}\nContent-Type: text/plain; charset=ISO-8859-1\n\n" + body
       smtp = Net::SMTP.new 'smtp.gmail.com', 587
       smtp.enable_starttls
       smtp.start("gmail.com", "guardian.test.ap", "ManicMonday#01", :login)
       smtp.send_message(msg,
                         "Guardian Test App",
                         * addresses)
       # *  (to.split(';').map { |x| x.strip.downcase }
       #     .find_all { |x| (x.length > 0) && (x.split('@').map { |x| x.strip }.find_all { |x| x.length > 0}.length == 2) }).uniq)
=end

  end
    
    def self.current_time
      Time.new
    end

    def self.has_location (checkin)
      (checkin.latitude && checkin.longitude)
    end

    def self.get_body_section_for_checkin (checkin, time_now)
      r = ((time_now - checkin.checkin_time) / 60).round.to_s + " minutes before this email\n"
      if (checkin.latitude && checkin.longitude)
        r += ("Latitude: " + checkin.latitude.to_s + ", Longitude: " + checkin.longitude.to_s + "\n")
        r += "http://maps.google.com/maps?q=" + checkin.latitude.to_s + "," + checkin.longitude.to_s + "\n"
      end
      r
    end

    def self.get_seconds_in_grant (user)
      if user.last_subscription_expiration_grant
        seconds_in_grant = (user.last_subscription_expiration_grant - GuardianUtil.current_time).floor
        seconds_in_grant = 0 if seconds_in_grant < 0
        seconds_in_grant
      else
        0
      end
    end

    def self.allowed_to_send_sms (watch)
      user = watch.guardian_user
      if (user.version_ordinal > 0)
        watch.subscription_valid_at_creation || ((self.get_seconds_in_grant user) > 0)
      else
        true
      end
    end

    def self.do_panic_for_watch (watch, manual_panic)
      watch.panic_send_time = self.current_time
      # send email here
      begin
        time_now = self.current_time
        subject = manual_panic ? "Manual panic sent!" : "Automatic Panic time struck!"
        name = watch.guardian_user.name_first + " " + watch.guardian_user.name_last

        # #!%#! ruby syntax pushing me to mutables
        subject += " for " + name
        subject += " " + watch.email_subject_line if watch.email_subject_line
=begin
        body = ""
        body += watch.email_body + "\n\n" if watch.email_body

        body += name + " set this warning " + self.get_body_section_for_checkin(watch.activation_guardian_checkin, time_now)

        body += ("\nLast interim checkin was at " + self.get_body_section_for_checkin(watch.interim_guardian_checkin, time_now)) if watch.interim_guardian_checkin
        body += ("\nManual panic was sent " + self.get_body_section_for_checkin(watch.deactivation_guardian_checkin, time_now)) if watch.deactivation_guardian_checkin
=end

        last_checkin_with_location = watch.activation_guardian_checkin
        last_checkin = watch.activation_guardian_checkin
        last_checkin = watch.interim_guardian_checkin if watch.interim_guardian_checkin
        last_checkin_with_location = last_checkin if self.has_location last_checkin
        last_checkin = watch.deactivation_guardian_checkin if watch.deactivation_guardian_checkin
        last_checkin_with_location = last_checkin if self.has_location last_checkin

        body = name + (manual_panic ? " has activated the panic button with the Guardian Trace App." : " has failed to check-in with their Guardian Trace App.")
        if self.has_location last_checkin_with_location
          body += "  Last known location: http://maps.google.com/maps?q=" + last_checkin_with_location.latitude.to_s + "," + last_checkin_with_location.longitude.to_s
        end

        all_recipients = watch.panic_email_addresses.split(';').map { |x| x.strip.downcase }.find_all { |x| (x.length > 0) }.uniq
        is_email = Proc.new { |x| (x.split('@').map { |x| x.strip }.find_all { |x| x.length > 0}.length == 2) }
        email_addresses = all_recipients.find_all { |x| is_email.call(x) }
        sms_targets = all_recipients.find_all { |x| ! is_email.call(x) }


        # lhf = last heard from
=begin
        lhf = watch.activation_guardian_checkin
        lhf = watch.interim_guardian_checkin if watch.interim_guardian_checkin
        lhf = watch.deactivation_guardian_checkin if watch.deactivation_guardian_checkin
        smsbody = "Emergency alert from " + watch.guardian_user.name_first[0..6] + " " + watch.guardian_user.name_last[0..6]
        if manual_panic
          smsbody += " Panic!"
        else
          smsbody += " last heard from " + ((time_now - lhf.checkin_time) / 60).round.to_s + " minutes ago"
        end

        if (lhf.latitude && lhf.longitude)
          smsbody += " from loc http://maps.google.com/maps?q=" + lhf.latitude.to_s + "," + lhf.longitude.to_s
        end
=end

        # panic_addon_at_end = (manual_panic ? "NOTE: Panic button is on Main Screen & Countdown Page" : "")
        puts "Sending SMS Messages"

        if self.allowed_to_send_sms watch
          sms_targets.each do |number|
            Twilio::RestAccount.new("ACe6b678e01708920760e1344780de2301", "6441fb0017a8c0b72fd74f4de6da62e4")
              .request("/2010-04-01/Accounts/ACe6b678e01708920760e1344780de2301/SMS/Messages",
                       "POST",
                       {'From' => '(512) 772-5411',
                         'To' => number, # '860-595-7687',
                         'Body' => body })
          end
        end

        puts "Sending Email Messages"
        self.send_email(
                        email_addresses, # watch.panic_email_addresses,
                        subject,
                        body + "\nPlease contact them immediately.\nTo learn more\nhttp://guardiantrace.com")
        
        
      rescue Exception => e
        # As bad as this is, we don't want to have a failed email system make us rethrow
        puts e.to_s
        begin
          self.send_email(
                          [watch.guardian_user.email_address],
                        "Email send error",
                        "Had a problem sending your panic email to recipients: " + e.to_s)
      rescue Exception => ie
        puts ie.to_s
      end
    end
  end

  def self.send_expired_watches
    rc = 0
    GuardianUser.transaction do
      time_now = self.current_time
      GuardianOutstanding.where(["deactivation_guardian_checkin_id is null AND panic_send_time is null and panic_deadline <= ?", time_now]).each do |watch|
        self.do_panic_for_watch watch, false
        watch.save
        rc = rc + 1
      end
    end
    rc
  end

  def self.validate_subscription receipt_data
    req = Net::HTTP::Post.new("/verifyReceipt") # , initheader = {'Content-Type' =>'application/json'})
    server = (Base64.decode64(receipt_data).include?("Sandbox")) ? "sandbox.itunes.apple.com" : "buy.itunes.apple.com"
    # server = "sandbox.itunes.apple.com"
    # req.basic_auth @user, @pass
    req.body =
      {
      "receipt-data" => receipt_data,
      "password" => "56f20cd497b3419a8d7aad1ae63e9cbe"
    }.to_json
    http = Net::HTTP.new(server, 443)
    http.use_ssl = true
    response = http.start {|http| http.request(req) }
    puts response
    if response.code == '200'
      ret = JSON.parse response.body
      ret
    else
      nil
    end
  end


  def self.test_validate_subscription #  receipt_data
    self.validate_subscription Base64.encode64('{
	"signature" = "ApO543I5X+oUDddSE3isxetrgJUJCx1BX71MdWppAeCQOin81HQDByuscSCZth6gWKe5+j8UsLCbrQMl2SDtXL+3vfVgjXNumnH+gn0P9BbZJujT/saXIcanyoZtQSCuM7an7DBwd30O+wz/ar35RynwqOzoCRL4z/EoAqk9Km47AAADVzCCA1MwggI7oAMCAQICCGUUkU3ZWAS1MA0GCSqGSIb3DQEBBQUAMH8xCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSYwJAYDVQQLDB1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEzMDEGA1UEAwwqQXBwbGUgaVR1bmVzIFN0b3JlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTA5MDYxNTIyMDU1NloXDTE0MDYxNDIyMDU1NlowZDEjMCEGA1UEAwwaUHVyY2hhc2VSZWNlaXB0Q2VydGlmaWNhdGUxGzAZBgNVBAsMEkFwcGxlIGlUdW5lcyBTdG9yZTETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMrRjF2ct4IrSdiTChaI0g8pwv/cmHs8p/RwV/rt/91XKVhNl4XIBimKjQQNfgHsDs6yju++DrKJE7uKsphMddKYfFE5rGXsAdBEjBwRIxexTevx3HLEFGAt1moKx509dhxtiIdDgJv2YaVs49B0uJvNdy6SMqNNLHsDLzDS9oZHAgMBAAGjcjBwMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUNh3o4p2C0gEYtTJrDtdDC5FYQzowDgYDVR0PAQH/BAQDAgeAMB0GA1UdDgQWBBSpg4PyGUjFPhJXCBTMzaN+mV8k9TAQBgoqhkiG92NkBgUBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAEaSbPjtmN4C/IB3QEpK32RxacCDXdVXAeVReS5FaZxc+t88pQP93BiAxvdW/3eTSMGY5FbeAYL3etqP5gm8wrFojX0ikyVRStQ+/AQ0KEjtqB07kLs9QUe8czR8UGfdM1EumV/UgvDd4NwNYxLQMg4WTQfgkQQVy8GXZwVHgbE/UC6Y7053pGXBk51NPM3woxhd3gSRLvXj+loHsStcTEqe9pBDpmG5+sk4tw+GK3GMeEN5/+e1QT9np/Kl1nj+aBw7C0xsy0bFnaAd1cSS6xdory/CUvM6gtKsmnOOdqTesbp0bs8sn6Wqs0C9dgcxRHuOMZ2tm8npLUm7argOSzQ==";
	"purchase-info" = "ewoJInF1YW50aXR5IiA9ICIxIjsKCSJwdXJjaGFzZS1kYXRlIiA9ICIyMDExLTEwLTMxIDAyOjM5OjU5IEV0Yy9HTVQiOwoJIml0ZW0taWQiID0gIjQ3NDA4MDY0NyI7CgkiZXhwaXJlcy1kYXRlLWZvcm1hdHRlZCIgPSAiMjAxMS0xMC0zMSAwMzozOTo1OSBFdGMvR01UIjsKCSJleHBpcmVzLWRhdGUiID0gIjEzMjAwMzIzOTk2MjQiOwoJInByb2R1Y3QtaWQiID0gImNvbS5iYXl0YW5sYWJzLnN1YnNjcjF5ZWFyIjsKCSJ0cmFuc2FjdGlvbi1pZCIgPSAiMTAwMDAwMDAxMTY0NDM4OCI7Cgkib3JpZ2luYWwtcHVyY2hhc2UtZGF0ZSIgPSAiMjAxMS0xMC0zMSAwMjo0MDowMSBFdGMvR01UIjsKCSJvcmlnaW5hbC10cmFuc2FjdGlvbi1pZCIgPSAiMTAwMDAwMDAxMTY0NDM4OCI7CgkiYmlkIiA9ICJjb20uYmF5dGFubGFicy5ndWFyZGlhbnRyYWNlIjsKCSJidnJzIiA9ICIxLjAiOwp9";
	"environment" = "Sandbox";
	"pod" = "100";
	"signing-status" = "0";
}')
  end

  def self.satisfy_subscription_requests
    GuardianUser.transaction do
      GuardianUser.where(:wants_new_subscription_query => true).each do |user|
        # Can't throw out of this, will generally want to update the user.
        validation_result = nil
        begin
          validation_result = self.validate_subscription user.last_clientsourced_receipt_base64
          next if validation_result == nil
        rescue
          next
        end
        lr = validation_result
        lr = validation_result["latest_receipt_info"] if validation_result["latest_receipt_info"]
        user.latest_receipt_base64 = validation_result["latest_receipt"]
        case validation_result["status"]
        when 0 # valid
          user.time_subscription_expiration_grant_updated = self.current_time
          duration_seconds = -86400 # unless we hear otherwise, mark this as dead for a day
          case lr["product_id"]
          when "com.baytanlabs.subscr1year"
            duration_seconds = 366 * 86400
          when "com.baytanlabs.subscr1month"
            duration_seconds = 31 * 86400
          else
          end
          user.time_subscription_expiration_grant_updated = self.current_time
          user.last_subscription_expiration_grant = Time.parse(lr["purchase_date"]) + duration_seconds
          user.grant_knowledge_update_count= user.grant_knowledge_update_count + 1
          user.wants_new_subscription_query = false
          user.save
        when 21006
          user.time_subscription_expiration_grant_updated = self.current_time
          user.last_subscription_expiration_grant = Time.at(0)
          user.grant_knowledge_update_count= user.grant_knowledge_update_count + 1
          user.wants_new_subscription_query = false
          user.save
        else
        end
      end
      0
    end
  end
end


