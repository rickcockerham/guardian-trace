function loadGeoloc(){
    if (navigator.geolocation) {
       var timeoutVal = 10 * 1000 * 1000;
       navigator.geolocation.getCurrentPosition(
            displayPosition,
            displayError,
           { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
       );
    }
    else {
        alert("Geolocation is not supported by this browser");
    }
}

function displayPosition(position) {
    populateInputs(position.coords.latitude,position.coords.longitude);

}

function displayError(error) {
    var errors = {
        1: 'Permission denied',
        2: 'Position unavailable',
        3: 'Request timeout'
    };
    alert("Error: " + errors[error.code]);
}

function populateInputs(lat,lng) {
    if (document.forms[0].outstanding_latitude) {
        document.getElementById("latitude").value=lat;
        document.getElementById("longitude").value=lng;
    }
    else {
        document.getElementById("latitude").value=lat;
        document.getElementById("longitude").value=lng;
    }
}