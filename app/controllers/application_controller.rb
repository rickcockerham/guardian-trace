class ApplicationController < ActionController::Base
  protect_from_forgery

  #before_filter :set_timezone
  before_filter :require_login

  private

  #def set_timezone
    # if settings.nil?
   # settings = GlobalSettings.first
    #  end
    # current_user.time_zone #=> 'London'
   # Time.zone = settings.time_zone
  #end

  def mobile_device?
    request.user_agent =~ /Mobile|webOS/
  end
  helper_method :mobile_device?


  def require_login
    unless current_user
      redirect_to new_session_path
    end
  end
  #helper_method :require_login

  def current_user
    @current_user ||= User.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
  end
  helper_method :current_user
end
