class OutstandingsController < ApplicationController
  before_filter :get_org
  before_filter :check_org, :only => :expired_index
  #before_filter :check_org, :only => :expired_index

  def get_org
    @organization = Organization.find(params[:organization_id])
  end

  def check_org
    if current_user.super_user != true
      @proper_org = Organization.find(current_user.organization)
    end

    #if current_user.admin != true && current_user.super_user != true
    #  redirect_to new_organization_outstanding_path(@proper_org)
    if @proper_org != @organization && current_user.super_user != true
      puts "wrong org"
      redirect_to organization_path(@proper_org)
    end
  end

  # GET /outstandings
  # GET /outstandings.json
  def index
    @outstandings = Outstanding.find(:all, :order => "panic_deadline DESC")


    respond_to do |format|
      format.html { render :layout => 'application_dashboard' }
      format.json { render json: @outstandings }
    end
  end

  # GET /outstandings
  # GET /outstandings.json
  def expired_index
    @outstandings = Outstanding.find(:all, :order => "panic_deadline DESC")
    respond_to do |format|
      format.html { render :layout => 'application_dashboard' }
      format.json { render json: @outstandings }
    end
  end

  # GET /outstandings/1
  # GET /outstandings/1.json
  def show
    @outstanding = Outstanding.find(params[:id])
    @user = User.find(@outstanding.user_id)


    respond_to do |format|
      format.html { render :layout => 'application_mobile_cnt' }
      format.json { render json: @outstanding }
    end
  end

  # GET /outstandings/new
  # GET /outstandings/new.json
  def new
    @outstanding = Outstanding.new
  #  my_user_id = current_user.id.to_i
  #  puts "Current User ID:" + current_user.id.to_i.to_s
  #  @user_assc_arr = UserToOutstAssc.where("user_id = ? AND active = ?", my_user_id, true)

  #  if @user_assc_arr.any?
  #    @user_assc = @user_assc_arr.last
  #    o_id = @user_assc.outstanding_id
  #
  #    @curr_outstanding = Outstanding.find(o_id)
    if current_user.active_outstanding
      @curr_outstanding = @organization.outstandings.find(current_user.active_outstanding)
      redirect_to organization_outstanding_path(@organization, @curr_outstanding)
    else
      respond_to do |format|
       format.html { render :layout => 'application_mobile_loc'}
        format.json { render json: @outstanding }
     end
    end
  end

  # GET /outstandings/1/edit
  def edit
    @outstanding = Outstanding.find(params[:id])
  end

  # POST /outstandings
  # POST /outstandings.json
  def create
    @outstanding = @organization.outstandings.new(params[:outstanding])
    @user = current_user
    @outstanding.user_id = @user.id

    respond_to do |format|
      if @outstanding.save
        format.html { redirect_to organization_outstanding_path(@organization, @outstanding), notice: 'Outstanding was successfully created.' }
        format.json { render json: @outstanding, status: :created, location: @outstanding }
      else
        format.html { render action: "new" }
        format.json { render json: @outstanding.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /outstandings/1
  # PUT /outstandings/1.json
  def update
    @outstanding = Outstanding.find(params[:id])

    respond_to do |format|
      if @outstanding.update_attributes(params[:outstanding])
        format.html { redirect_to organization_outstanding_path(@organization, @outstanding), notice: 'Outstanding was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @outstanding.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /outstandings/1
  # DELETE /outstandings/1.json
  def destroy
    @outstanding = Outstanding.find(params[:id])
    @outstanding.destroy

    respond_to do |format|
      format.html { redirect_to expired_path }
      format.json { head :no_content }
    end
  end

  def sent_alert
    put "Sent Alert"
    #@outstanding = Outstanding.find(params[:id])
    #@outstanding.sent_alert
    #outst_rel = OutstToJobRelation.find(@outstanding.id)
    #unless outst_rel.panic?
      render :layout => 'alert'
    #end
  end

  def panic_alert
    puts "Panic Alert"
    @outstanding = Outstanding.find(params[:id])
    @outstanding.panic_alert

    render :layout => 'alert'
  end

  def cancel_alert
    @outstanding = Outstanding.find(params[:id])
    @outstanding.cancel_alert
    if mobile_device?
      redirect_to new_organization_outstanding_path(@organization)
    else
      redirect_to organization_outstandings_path(@organization)
    end
  end

  def pin_cancel
    @outstanding = Outstanding.find(params[:id])
    render :layout => 'application_mobile_pin'
  end

end
