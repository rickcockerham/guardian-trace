class SessionsController < ApplicationController
  def load_organization
    @organization = Organization.find(current_user.organization_id)
  end

  skip_before_filter :require_login

  def new
    if current_user && current_user.organization_id != nil
      load_organization
      puts "current user not nil"
      if mobile_device?
        if current_user.registered?
          puts "registered"
          if current_user.active_outstanding
            redirect_to organization_outstanding_path(@organization, current_user.active_outstanding)
          else
            redirect_to new_organization_outstanding_path(@organization)
          end
        else
          puts "not registered"
          #redirect_to  :controller => 'users', :action => "set_pin", :id => current_user.id
          redirect_to set_pin_organization_user_path(@organization, current_user.id)
        end
      else
        if current_user.registered?
          redirect_to organization_outstandings_path(@organization)
       else
          redirect_to set_pin_organization_user_path(@organization, current_user.id)
       end
      end
    elsif current_user && current_user.organization_id.nil?
       redirect_to organizations_path
    else
      puts "current user nil"
      if mobile_device?
        render :layout => 'application_mobile'
      else
        render :layout => 'dashboard_login', :action => "new_admin"
      end

    end
  end

  def new_admin
    if current_user
      load_organization
      redirect_to organization_outstandings_path(@organization)
    else
      render :layout => 'dashboard_login'
    end
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      #if params[:remember_me]
        cookies.permanent[:auth_token] = user.auth_token
      #else
      #  cookies[:auth_token] = user.auth_token
      #end
      if current_user.organization_id !=nil
        load_organization

        if mobile_device?
          if current_user.registered?
            redirect_to new_organization_outstanding_path(@organization)
          else
            #redirect_to  :controller => 'users', :action => "set_pin", :id => current_user.id
            redirect_to set_pin_organization_user_path(@organization, current_user.id)
          end
        else
          if current_user.registered?
            redirect_to organization_outstandings_path(@organization)
          else
            redirect_to set_pin_organization_user_path(@organization, current_user.id)
          end
        end
      elsif current_user.organization_id.nil? && current_user.super_user?
        redirect_to organizations_path
      else
        redirect_to logout_path
      end
    else
      if mobile_device?
        render :layout => 'application_mobile'
      else
        render :layout => 'dashboard_login', :action => "new_admin"
      end
    end
  end

  #def create_admin
  #  user = User.find_by_email(params[:email])
  #  if user && user.authenticate(params[:password])
  #    #if params[:remember_me]
  #    cookies.permanent[:auth_token] = user.auth_token
  #    #else
  #    #  cookies[:auth_token] = user.auth_token
  #    #end
  #    redirect_to :controller => 'outstandings', :action => 'index'
  #  else
  #    flash.now.alert = "Invalid email or password"
     # render "new"
  #  end
  #end

  def destroy
    cookies.delete(:auth_token)
   # if mobile_device?
      redirect_to root_url, :notice => "Logged out!"
   # else
   #   redirect_to new_admin_session_path
   # end
  end

  #def admin_destroy
  #  cookies.delete(:auth_token)
  #  redirect_to :controller => 'sessions', :action => 'new_admin'
  #end
end
