class UsersController < ApplicationController
  before_filter :get_org
  before_filter :check_org, :only => [:index, :edit, :new, :destroy]

  def get_org
    @organization = Organization.find(params[:organization_id])
  end

  def check_org
    if current_user.super_user != true
      @proper_org = Organization.find(current_user.organization)
    end

    if current_user.admin == false && current_user.super_user == false
      redirect_to new_organization_outstanding_path(@proper_org)
    elsif current_user.organization_id != @organization.id && current_user.super_user == false
      redirect_to organization_path(@proper_org)
    end
  end

  # GET /users
  # GET /users.json
  def index

    @users = User.all

    respond_to do |format|
      format.html { render :layout => 'application_dashboard' }
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = @organization.users.find(params[:id])

    respond_to do |format|
      format.html { render :layout => 'application_dashboard' }
      format.json { render json: @user }
    end
  end

  #GET /users/1/edit
  #GET /users/1/edit.json
  def edit

    @user = @organization.users.find(params[:id])

    respond_to do |format|
      format.html { render :layout => 'application_dashboard' }
      format.json { render json: @user }
    end
  end

  #GET /users/1/set_pin
  #GET /users/1/set_pin.json
  def set_pin
    @user = User.find(params[:id])
    if mobile_device?
      render :layout => 'application_mobile_set_pin'
    else
      render :layout => 'dashboard_set_pin'
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new

    @user = User.new
    render :layout => 'application_dashboard'
  end

  # POST /users
  # POST /users.json
  def create
    @user = @organization.users.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html { redirect_to organization_users_path(@organization), notice: 'Success. User Created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new", layout: 'application_dashboard', notice: 'Error. Incorrect Information.'  }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        if mobile_device?
          format.html { redirect_to new_organization_outstanding_path(@organization) }
        else
          format.html { redirect_to organization_user_path(@organization, @user), notice: 'User was successfully updated.' }
          format.json { head :no_content }
        end
     else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def save_pin
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to :root_path, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "save_pin" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

    redirect_to :root_path
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy

    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to organization_users_path(@organization) }
      format.json { head :no_content }
    end
  end
end
