class SettingsController < ApplicationController
  before_filter :get_org
  before_filter :check_org, :only => :edit

  def get_org
    @organization = Organization.find(params[:organization_id])
  end

  def check_org
    if current_user.super_user != true
      @proper_org = Organization.find(current_user.organization)
    end

    if current_user.admin == false && current_user.super_user == false
      redirect_to new_organization_outstanding_path(@proper_org)
    elsif current_user.organization_id != @organization.id && current_user.super_user == false
      redirect_to organization_path(@proper_org)
    end
  end

  # GET /settings
  # GET /settings.json
  def index
    @settings = Setting.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @settings }
    end
  end

  # GET /settings/1
  # GET /settings/1.json
  def show
    @setting = Setting.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @setting }
    end
  end

  # GET /settings/new
  # GET /settings/new.json
  def new
    @setting = Setting.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @setting }
    end
  end

  # GET /settings/1/edit
  def edit
    @setting = @organization.setting
    @admin_users = @organization.users.find(:all, :conditions => ['admin = true'])
    render :layout => 'application_dashboard'
  end

  # POST /settings        :controller => 'outstandings', :action => 'new'
  # POST /settings.json
  def create
    @setting = @organization.setting.new(params[:setting])

    respond_to do |format|
      if @setting.save
        format.html { redirect_to @setting, notice: 'Setting was successfully created.' }
        format.json { render json: @setting, status: :created, location: @setting }
      else
        format.html { render action: "new" }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /settings/1
  # PUT /settings/1.json
  def update
    @setting = Setting.find(params[:id])


    respond_to do |format|
      if @setting.update_attributes(params[:setting])
        if @setting.delete_logo == true
          @setting.dashboard_logo = nil
          @setting.save
        end
        format.html { redirect_to edit_organization_setting_path(@organization, @setting), notice: 'Settings were successfully saved.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /settings/1
  # DELETE /settings/1.json
  def destroy
    @setting = Setting.find(params[:id])
    @setting.destroy

    respond_to do |format|
      format.html { redirect_to settings_url }
      format.json { head :no_content }
    end
  end
end
