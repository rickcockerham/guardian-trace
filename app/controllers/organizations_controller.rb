class OrganizationsController < ApplicationController
  before_filter :check_access, :except => [:show, :create, :update]

  def check_access
    puts "check_access"
    unless current_user.organization_id == nil
      puts "access denied"
      @org = Organization.find(current_user.organization_id)
      redirect_to organization_path(@org)
    end
  end

  # GET /organizations
  # GET /organizations.json
  def index
    @organizations = Organization.find(:all, :conditions => ['active = true'])


    respond_to do |format|
      format.html { render :layout => 'application_dashboard_orgs' }
      format.json { render json: @organizations }
    end
  end

  def index_active_orgs
    @organizations = Organization.all

    respond_to do |format|
      format.html { render :layout => 'application_dashboard_orgs' }
      format.json { render json: @organizations }
    end
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
    @organization = Organization.find(params[:id])

    respond_to do |format|
      format.html {
        if current_user.super_user !=true && current_user.organization_id != @organization.id
          @user_org = Organization.find(current_user.organization_id)
          redirect_to organization_outstandings_path(@user_org)
        else
          redirect_to organization_outstandings_path(@organization)
        end
      }
      format.json { render json: @organization }
    end
  end

  # GET /organizations/new
  # GET /organizations/new.json
  def new
    @organization = Organization.new
    @organization.build_setting
    @organization.users.build


    respond_to do |format|
      format.html { render :layout => 'application_dashboard_orgs' }
      format.json { render json: @organization }
    end
  end

  # GET /organizations/1/edit
  def edit
    @organization = Organization.find(params[:id])
    @admin =  @organization.users.find(@organization.setting.user_id)

    respond_to do |format|
      format.html { render :layout => 'application_dashboard_orgs' }
      format.json { render json: @organization }
    end
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(params[:organization])

    #@admin_user = @organization.users.first
    #puts @admin_user.name_first + @admin_user.name_last
    #@settings = Setting.create!

       # create(green_hours: 99, green_mins: 59, yellow_hours: 0, yellow_mins: 15, red_hours: 0,
       #                          red_mins: 0, reminder_sms: 't', reminder_sms_hours: 0, reminder_sms_mins: 15,
       #                          history_hours: 24)
    #@settings.user_id = @admin_user.id
    #@settings.save

    respond_to do |format|
      if @organization.save

        format.html { redirect_to organizations_path, notice: 'Organization was successfully created.' }
        format.json { render json: @organization, status: :created, location: @organization }
      else
        format.html { render action: "new" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /organizations/1
  # PUT /organizations/1.json
  def update
    @organization = Organization.find(params[:id])

    respond_to do |format|
      if @organization.update_attributes(params[:organization])
        if @organization.setting.delete_logo == true
          @organization.setting.dashboard_logo = nil
          @organization.setting.save
        end
        format.html { redirect_to organizations_path, notice: 'Organization was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    @organization = Organization.find(params[:id])
    @organization.destroy

    respond_to do |format|
      format.html { redirect_to organizations_url }
      format.json { head :no_content }
    end
  end

  def deactivate_account
    @organization = Organization.find(params[:id])
    @organization.deactivate_account
    redirect_to organizations_path
  end
end
