class SaveJobs < Struct.new(:outst_id)
  def perform
    wait_and_save(outst_id)

  end

  def wait_and_save(ost_id)
    ost = Oustanding.find(ost_id)
    ost.active=false
    ost.panic_send_time=Time.now
    puts "Wait and save"
    sleep(2)
    ost.save
  end

end