class Setting < ActiveRecord::Base
  attr_accessible :green_hours, :green_mins, :yellow_hours, :yellow_mins, :red_hours, :red_mins, :reminder_sms,
                  :reminder_sms_hours, :reminder_sms_mins, :logo_url, :comp_name, :alert_emails, :alert_smss,
                  :history_hours, :time_zone, :user_id, :organization_id, :dashboard_logo, :startup_image, :icon, :delete_logo

  belongs_to :organization

  has_attached_file :dashboard_logo, :styles => { :medium => "300x90>"},
                    :url => "/assets/paperclip/settings/:id/dashboard_logo/:style/:basename.:extension",
                    :path => ":rails_root/public/assets/paperclip/settings/:id/dashboard_logo/:style/:basename.:extension"
  has_attached_file :startup_image, :styles => { :medium => "320x460"},
                    :url => "/assets/paperclip/settings/:id/startup_image/:style/startup.png",
                    :path => ":rails_root/public/assets/paperclip/settings/:id/startup_image/:style/startup.png"
  has_attached_file :icon, :styles => { :small => "57x57", :medium => "72x72", :large => "114x114"},
                    :url => "/assets/paperclip/settings/:id/icon/:style/apple-touch-icon-precomposed.png",
                    :path => ":rails_root/public/assets/paperclip/settings/:id/icon/:style/apple-touch-icon-precomposed.png"

  validates_attachment :dashboard_logo, :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  validates_attachment :startup_image, :content_type => { :content_type => ["image/png"] }
  validates_attachment :icon, :content_type => { :content_type => ["image/png"] }

end
