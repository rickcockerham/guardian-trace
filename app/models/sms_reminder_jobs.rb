class SmsReminderJobs < Struct.new(:outst_id, :assc_id)
  def perform
    send_sms(outst_id, assc_id)

  end

  def send_sms(outst_id, assc_id)
    outstanding = Outstanding.find(outst_id)
    user = User.find(outstanding.user_id)
    org = Organization.find(outstanding.organization_id)
    settings = org.setting

    asscRem = OutstToJobRelation.find_by_outstanding_id(assc_id)
    #asscRem.delete

    number = user.sms
    time_now = Time.now

    smsbody = "This is a reminder that your outstanding alert is scheduled to expire in " + settings.reminder_sms_hours.to_s
    smsbody += " hours and " + settings.reminder_sms_mins.to_s + " minutes."

    account_sid = "ACc69ddb3ba296de4456ab3b79d1b646a4"
    auth_token = "ac7b1399ba45327cbc2e1f87b9247bcc"
    from = '+15126237510'
    client = Twilio::REST::Client.new account_sid, auth_token

    client.account.messages.create(
        :from => from,
        :to => number,
        :body => smsbody
    )
    puts "Sent message to #{number}"

=begin
    require 'twiliolib'


      Twilio::RestAccount.new("ACe6b678e01708920760e1344780de2301", "6441fb0017a8c0b72fd74f4de6da62e4")
      .request("/2010-04-01/Accounts/ACe6b678e01708920760e1344780de2301/SMS/Messages",
               "POST",
               {'From' => '(512) 772-5411',
                'To' => number,
                'Body' => smsbody })
=end

  end
end