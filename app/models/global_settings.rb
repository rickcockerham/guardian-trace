class GlobalSettings < ActiveRecord::Base
  attr_accessible :theme_color, :theme_color_contrast, :brand_image, :time_zone
end
