class EmailJobs < Struct.new(:outst_id)
  def perform
    send_email(outst_id)

  end

  def send_email(outst_id)
    require 'net/smtp'

    outstanding = Outstanding.find(outst_id)
    assc = OutstToJobRelation.find_by_outstanding_id(outstanding.id)
    user = User.find(outstanding.user_id)
    org = Organization.find(outstanding.organization_id)
    settings = org.setting

    subject = assc.panic ? "Manual panic sent!" : "Automatic Panic time struck!"
    #addresses = settings.alert_emails
    #addresses = Array["tasslehawf@mac.com"]
    #addresses = Array["mikedavis@baytanlabs.com", "tasslehawf@mac.com","EstellaBaytan@BaytanLabs.com"]
    addresses = Array[settings.alert_emails]
    name = user.name_first + ' ' + user.name_last

    body = name + (outstanding.manual_panic ? " has activated the panic button with the Guardian Trace App." : " has failed to check-in with their Guardian Trace App.")
    if (outstanding.latitude && outstanding.longitude)
      body += "  Last known location: http://maps.google.com/maps?q=" + outstanding.latitude.to_s + "," + outstanding.longitude.to_s
    end
    #body += " OST" + outst_id.to_s


    Net::SMTP.start(
        'smtp.1and1.com',
        25,
        'guardiantrace.com',
        'alert@guardiantrace.com',
        'Pioneer1',
        :plain)  do |smtp|

      smtp.open_message_stream('Alert@GuardianTrace.com', addresses) do |f|
        f.puts 'From: Alert@GuardianTrace.com'
        f.puts 'To: ' + addresses.map {|e| ["; ", e]}.flatten(1).drop(1).join
        f.puts 'Subject: ' + subject
        f.puts ''
        f.puts body
      end
    end
  end
end