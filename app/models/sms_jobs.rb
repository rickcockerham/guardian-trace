class SmsJobs < Struct.new(:outst_id)
  def perform
    send_sms(outst_id)

  end

  def send_sms(outst_id)
    outstanding = Outstanding.find(outst_id)
    user = User.find(outstanding.user_id)
    org = Organization.find(outstanding.organization_id)
    settings = org.setting

    #sms_targets = settings.alert_smss
    #sms_targets = Array['5123007142']
    #sms_targets = Array['5123007142','5123007142','5123007142','4083988333']
    sms_targets = Array[settings.alert_smss]

    time_now = Time.now
    smsbody = "Emergency alert from " + user.name_first[0..6] + " " + user.name_last[0..6]
    smsbody += " last heard from " + ((time_now - outstanding.panic_deadline) / 60).round.to_s + " minutes ago"
    if (outstanding.latitude && outstanding.longitude)
      smsbody += " from loc http://maps.google.com/maps?q=" + outstanding.latitude.to_s + "," + outstanding.longitude.to_s
    end
    #smsbody += " OST" + outst_id.to_s

    #require 'twiliolib'

    sms_targets.each do |number|

      account_sid = "ACc69ddb3ba296de4456ab3b79d1b646a4"
      auth_token = "ac7b1399ba45327cbc2e1f87b9247bcc"
      from = '+15126237510'
      client = Twilio::REST::Client.new account_sid, auth_token

      client.account.messages.create(
          :from => from,
          :to => number,
          :body => smsbody
      )
      puts "Sent message to #{number}"
      
=begin
      Twilio::RestAccount.new("ACe6b678e01708920760e1344780de2301", "6441fb0017a8c0b72fd74f4de6da62e4")
      .request("/2010-04-01/Accounts/ACe6b678e01708920760e1344780de2301/SMS/Messages",
               "POST",
               {'From' => '(512) 772-5411',
                'To' => number,
                'Body' => smsbody })
=end
    end
  end

end