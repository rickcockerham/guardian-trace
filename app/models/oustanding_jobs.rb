#class OustandingJobs < ActiveRecord::Base
class OustandingJobs < Struct.new(:outst_id)
  def perform
    send_expired(outst_id)

  end

  def extend

  end



  def send_expired(id)
    outstanding = Outstanding.find(id)
    #user = User.find(outstanding.user_id)

    outstanding.sent_alert

    user_assc = UserToOutstAssc.find_by_outstanding_id(id)
    if user_assc
      user_assc.active = false
      user_assc.save
    end


    Delayed::Job.enqueue(SmsJobs.new(id))
    Delayed::Job.enqueue(EmailJobs.new(id))

    #send_sms(outstanding, user)
    #send_email(outstanding, user)

    #Delayed::Job.enqueue(SaveJobs.new(outstanding.id), run_at: Time.now)
  end

  def send_sms(outstanding, user)
    sms_targets = Array['5123007142']
    #sms_targets = Array['5123007142','5123007142','5123007142','4083988333']

    time_now = Time.now
    smsbody = "Emergency alert from " + user.name_first[0..6] + " " + user.name_last[0..6]
    smsbody += " last heard from " + ((time_now - outstanding.panic_deadline) / 60).round.to_s + " minutes ago"
    if (outstanding.latitude && outstanding.longitude)
      smsbody += " from loc http://maps.google.com/maps?q=" + outstanding.latitude.to_s + "," + outstanding.longitude.to_s
    end

    require 'twiliolib'

      sms_targets.each do |number|
        Twilio::RestAccount.new("ACe6b678e01708920760e1344780de2301", "6441fb0017a8c0b72fd74f4de6da62e4")
        .request("/2010-04-01/Accounts/ACe6b678e01708920760e1344780de2301/SMS/Messages",
             "POST",
             {'From' => '(512) 772-5411',
              'To' => number,
              'Body' => smsbody })
      end
  end

  def send_email(outstanding, user)
    require 'net/smtp'

    subject = outstanding.manual_panic ? "Manual panic sent!" : "Automatic Panic time struck!"
    addresses = Array["tasslehawf@mac.com"]
    #addresses = Array["mikedavis@baytanlabs.com", "tasslehawf@mac.com","EstellaBaytan@BaytanLabs.com"]
    name = user.name_first + ' ' + user.name_last

    body = name + (outstanding.manual_panic ? " has activated the panic button with the Guardian Trace App." : " has failed to check-in with their Guardian Trace App.")
    if (outstanding.latitude && outstanding.longitude)
      body += "  Last known location: http://maps.google.com/maps?q=" + outstanding.latitude.to_s + "," + outstanding.longitude.to_s
    end


    Net::SMTP.start(
        'smtp.1and1.com',
        25,
        'guardiantrace.com',
        'alert@guardiantrace.com',
        'Pioneer1',
        :plain)  do |smtp|

      smtp.open_message_stream('Alert@GuardianTrace.com', addresses) do |f|
        f.puts 'From: Alert@GuardianTrace.com'
        f.puts 'To: ' + addresses.map {|e| ["; ", e]}.flatten(1).drop(1).join
        f.puts 'Subject: ' + subject
        f.puts ''
        f.puts body
      end
    end
  end

end
