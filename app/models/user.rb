class User < ActiveRecord::Base
  attr_accessible :email, :password, :password_confirmation, :name_first, :name_last, :admin, :notes,
                  :current_outstanding, :pin, :registered, :sms, :super_user, :organization_id, :send_pw_setup_email
  has_secure_password
  belongs_to :organization
  has_many :outstandings, dependent: :destroy
  validates_presence_of :password, :on => :create
  validates_presence_of :email, :on => :create
  validates_uniqueness_of :email


  before_create do
    generate_token(:auth_token)
    #self.registered = false

  end

  after_update do

    if self.registered == false
      send_pin_setup_email
    end
  end

  #before_validation(:on => :update) do
  #  if self.registered?
  #  else
  #    self.registered=true
  #  end
  #end

  after_create do
    if self.registered == false
      send_pin_setup_email
    end
    #self.registered = false
    #self.save
  end

  def concat_name
    return self.name_last + self.name_first
  end

  def active_outstanding
    user_assc_arr = UserToOutstAssc.where("user_id = ? AND active = ?", self.id, true)

    if user_assc_arr.any?
      user_assc =  user_assc_arr.last
      return user_assc.outstanding_id
    else
      return false
    end
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def send_pin_setup_email
    Delayed::Job.enqueue(PinSetupJobs.new(self.id), :run_at => Time.now)
  end

  #def save_pin(pin_string)
  #  self.pin = pin_string
  #  self.save
  #end
end
