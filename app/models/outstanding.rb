class Outstanding < ActiveRecord::Base
  attr_accessible :deadline_hours, :deadline_mins, :panic_deadline, :panic_send_time, :user_id, :active, :manual_panic,
                  :latitude, :longitude, :panic_deadline_holding, :longitude_holding, :latitude_holding,
                  :deadline_hours_holding, :deadline_mins_holding, :first, :status, :organization_id
  belongs_to :organization
  has_many :checkins, dependent: :destroy

  attr_accessor :remaining_seconds
  attr_accessor :remaining_mins
  attr_accessor :remaining_hours

  attr_accessor :settings

  def time_remaining
    totalSeconds = (self.panic_deadline - Time.now).to_i
    self.remaining_seconds = totalSeconds % 60
    self.remaining_hours = ((totalSeconds - self.remaining_seconds) / 3600)
    self.remaining_mins = ((totalSeconds - (self.remaining_hours * 3600) - self.remaining_seconds) / 60)
  end


  def history_cutoff(org)
    #if settings.nil?
    #  settings = org.setting
    #end
    outst_rel = UserToOutstAssc.find_by_outstanding_id(self.id)
    puts "Outstanding Relation" + outst_rel.to_s
    seconds = (self.panic_deadline - Time.now)

    if seconds > -(org.setting.history_hours * 3600) && outst_rel.active==false
       return true
    else
      return false
    end
  end


   def status_image(org)
     if self.settings.nil?
     self.settings = org.setting
     end
     yellow = (self.settings.yellow_mins * 60) + (self.settings.yellow_hours * 3600)
     red = (self.settings.red_mins * 60) + (self.settings.red_hours * 3600)
     seconds = (self.panic_deadline - Time.now).to_i
     if seconds > yellow
       self.status = '/assets/images/stoplight-green.jpg'
      else
        (seconds < yellow) && (seconds > red) ? self.status = '/assets/images/stoplight-yellow.jpg' : self.status = '/assets/images/stoplight-red.jpg'
     end
   end


  after_create do
    #self.delay(run_at:self.panic_deadline).send_expired(self.id)
    #self.delay(run_at:self.panic_deadline).print_shit

    @organization = Organization.find(self.organization_id)

    if self.settings.nil?
      self.settings = @organization.setting
    end

    reminder_deadline = self.panic_deadline - self.settings.reminder_sms_hours.hours
    reminder_deadline = reminder_deadline - self.settings.reminder_sms_mins.minutes
    puts "Reminder SMS time" + reminder_deadline.to_i.to_s + "Deadline" + self.panic_deadline.to_i.to_s



    user_assc = UserToOutstAssc.new
    user_assc.user_id = self.user_id
    user_assc.outstanding_id = self.id
    user_assc.active = true
    user_assc.save

    del = Delayed::Job.enqueue(OustandingJobs.new(self), :run_at => self.panic_deadline)
    assc = OutstToJobRelation.new
    assc.outstanding_id = self.id
    assc.delayed_job_id = del.id
    assc.save

    #if self.settings.reminder_sms == true
    if reminder_deadline.to_i > Time.now.to_i
      delRem = Delayed::Job.enqueue(SmsReminderJobs.new(self.id, (self.id*10)), :run_at => reminder_deadline)
      asscRem = OutstToJobRelation.new
      asscRem.outstanding_id = self.id * 10
      asscRem.delayed_job_id = delRem.id
      asscRem.save
    end
    #end


  end

  before_validation(:on => :update) do
    puts "On update panic_deadline:" + self.panic_deadline.to_s + "Time now:" + Time.now.to_s

    if self.panic_deadline > Time.now # || self.active==true

    self.plus_hours = self.deadline_hours
    self.plus_minutes = self.deadline_mins

    if self.checkins.any?
      Checkin.create(
          :checkin_time => Time.now,
          :latitude => self.latitude,
          :longitude => self.longitude,
          :checkin_hours => self.plus_hours,
          :checkin_mins => self.plus_minutes,
          :outstanding_id => self.id
      )
    else
      Checkin.create(
          :checkin_time => self.panic_deadline_holding,
          :latitude => self.latitude_holding,
          :longitude => self.longitude_holding,
          :checkin_hours => self.deadline_hours_holding,
          :checkin_mins => self.deadline_mins_holding,
          :outstanding_id => self.id,
          :first => true
      )
      Checkin.create(
          :checkin_time => Time.now,
          :latitude => self.latitude,
          :longitude => self.longitude,
          :checkin_hours => self.plus_hours,
          :checkin_mins => self.plus_minutes,
          :outstanding_id => self.id
      )
    end


    self.deadline_hours_holding = self.deadline_hours_holding + self.plus_hours
    self.deadline_mins_holding = self.deadline_mins_holding + self.plus_minutes

    self.deadline_hours = self.deadline_hours_holding
    self.deadline_mins = self.deadline_mins_holding

    self.panic_deadline = self.panic_deadline + self.plus_hours.hours
    self.panic_deadline = self.panic_deadline + self.plus_minutes.minutes

    self.active = true

    self.extend_alert
    else
      self.active=false
    end
  end


  before_validation(:on => :create) do

    if self.deadline_hours.nil?
      self.deadline_hours=0
    end
    if self.deadline_mins.nil?
      self.deadline_mins=0
    end

    self.panic_deadline = Time.now
    self.panic_deadline = self.panic_deadline + self.deadline_hours.hours
    self.panic_deadline = self.panic_deadline + self.deadline_mins.minutes

    self.panic_deadline_holding = self.panic_deadline
    self.latitude_holding = self.latitude
    self.longitude_holding = self.longitude
    self.deadline_hours_holding = self.deadline_hours
    self.deadline_mins_holding = self.deadline_mins

    self.active = true

   # self.delay(run_at:self.panic_deadline).send_expired

  end

  def sent_alert
    #self.active=false
    #self.save
  end

  def is_active
    user_assc = UserToOutstAssc.find_by_outstanding_id(self.id)
    if user_assc
      return user_assc.active?
    else
      return false
    end
  end

  def cancel_alert
    assc = OutstToJobRelation.find_by_outstanding_id(self.id)
    if assc
      job =  Delayed::Job.find_by_id(assc.delayed_job_id)
      if job
        job.delete
      end
      #assc.delete
    end

    asscRem = OutstToJobRelation.find_by_outstanding_id(self.id*10)
    user_assc = UserToOutstAssc.find_by_outstanding_id(self.id)
    if asscRem
      rem_job = Delayed::Job.find_by_id(asscRem.delayed_job_id)
      if rem_job
        rem_job.delete
      end
      #asscRem.delete
    end
    user_assc.active = false
    user_assc.save


    #self.panic_deadline = Time.now
    #self.active=false
    #self.save

  end

  def extend_alert
    assc = OutstToJobRelation.find_by_outstanding_id(self.id)
    job =  Delayed::Job.find_by_id(assc.delayed_job_id)
    job.delete

    del = Delayed::Job.enqueue(OustandingJobs.new(self), :run_at => self.panic_deadline)
    assc = OutstToJobRelation.new
    assc.outstanding_id = self.id
    assc.delayed_job_id = del.id
    assc.save
  end

  def panic_alert
    assc = OutstToJobRelation.find_by_outstanding_id(self.id)
    if assc
      assc.panic=true
      assc.save
    end
    job =  Delayed::Job.find_by_id(assc.delayed_job_id)
    job.delete
    #assc.delete

    asscRem = OutstToJobRelation.find_by_outstanding_id(self.id*10)
    user_assc = UserToOutstAssc.find_by_outstanding_id(self.id)
    if asscRem
      rem_job = Delayed::Job.find_by_id(asscRem.delayed_job_id)
      if rem_job
        rem_job.delete
      end
      #asscRem.delete
    end

    user_assc.active = false
    user_assc.save



    Delayed::Job.enqueue(OustandingJobs.new(self), :run_at => Time.now)
    #self.panic_deadline = Time.now
    #self.active=false
    #self.save
  end

  def print_shit
    puts "BlahBlahBlahBlahBlahBlahBlah"
  end

  def check_not_expired
    #puts "Check Expired"
    user_assc = UserToOutstAssc.find_by_outstanding_id(self.id)
    if user_assc
      if self.panic_deadline <= Time.now
        user_assc.active = false
        user_assc.save
      end
    end
    #puts "Check Expired"
    if self.panic_deadline > Time.now && user_assc.active==true
      return true
    else
      return false
    end
  end

  def send_expired(ost_id)
    puts "Send Expired"
    ost = Outstanding.find(ost_id)
    user = User.find(ost.user_id)
    #self.send_sms(user)
    #self.send_email(user)

    require 'twiliolib'

    time_now = ost.panic_send_time

    smsbody = "Emergency alert from " + user.name_first[0..6] + " " + user.name_last[0..6]
    smsbody += " last heard from " + ((time_now - ost.panic_deadline) / 60).round.to_s + " minutes ago"
    if (ost.latitude && ost.longitude)
      smsbody += " from loc http://maps.google.com/maps?q=" + ost.latitude.to_s + "," + ost.longitude.to_s
    end

    number = '860-595-7687'
    number1 = '512-300-7142'
    body =  'This is the Guardian Web App calling you from beyond'

    Twilio::RestAccount.new("ACe6b678e01708920760e1344780de2301", "6441fb0017a8c0b72fd74f4de6da62e4")
    .request("/2010-04-01/Accounts/ACe6b678e01708920760e1344780de2301/SMS/Messages",
             "POST",
             {'From' => '(512) 772-5411',
              'To' => number1, # '860-595-7687',
              'Body' => smsbody })


    ost.active=false
    ost.panic_send_time=Time.now
    #ost.delay.wait_and_save(ost_id)


  end

  def wait_and_save(ost_id)
    ost = Oustanding.find(ost_id)
    puts "Wait and save"
    sleep(2)
    ost_id.save
  end



  def send_sms(sms_user)
    require 'twiliolib'

    time_now = self.panic_send_time

    smsbody = "Emergency alert from " + sms_user.name_first[0..6] + " " + sms_user.name_last[0..6]
    smsbody += " last heard from " + ((time_now - self.panic_deadline) / 60).round.to_s + " minutes ago"
    if (self.latitude && self.longitude)
      smsbody += " from loc http://maps.google.com/maps?q=" + self.latitude.to_s + "," + self.longitude.to_s
    end

    number = '860-595-7687'
    number1 = '512-300-7142'
    body =  'This is the Guardian Web App calling you from beyond'

    Twilio::RestAccount.new("ACe6b678e01708920760e1344780de2301", "6441fb0017a8c0b72fd74f4de6da62e4")
    .request("/2010-04-01/Accounts/ACe6b678e01708920760e1344780de2301/SMS/Messages",
             "POST",
             {'From' => '(512) 772-5411',
              'To' => number1, # '860-595-7687',
              'Body' => smsbody })
  end

  def send_email(email_user)
    require 'net/smtp'

    subject = manual_panic ? "Manual panic sent!" : "Automatic Panic time struck!"
    addresses = ["tasslehawf@mac.com"]
    addresses1 = ["mikedavis@baytanlabs.com", "tasslehawf@mac.com"]
    name = email_user.name_first + ' ' + email_user.name_last

    body = name + (manual_panic ? " has activated the panic button with the Guardian Trace App." : " has failed to check-in with their Guardian Trace App.")
    if (self.latitude && self.longitude)
      body += "  Last known location: http://maps.google.com/maps?q=" + self.latitude.to_s + "," + self.longitude.to_s
    end


    Net::SMTP.start(
        'smtp.1and1.com',
        25,
        'guardiantrace.com',
        'alert@guardiantrace.com',
        'Pioneer1',
        :plain)  do |smtp|

      smtp.open_message_stream('Alert@GuardianTrace.com', addresses) do |f|
        f.puts 'From: Alert@GuardianTrace.com'
        f.puts 'To: ' + addresses.map {|e| [";", e]}.flatten(1).drop(1).join
        f.puts 'Subject: ' + subject
        f.puts ''
        f.puts body
      end
    end
  end

end
