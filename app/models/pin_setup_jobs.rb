class PinSetupJobs < Struct.new(:user_id)
  def perform
    send_email(user_id)

  end

  def send_email(user_id)
    require 'net/smtp'

    user = User.find(user_id)

    subject = "Select Your new Guardian Trace PIN"
    addresses = Array[user.email]

    body = "A Guardian Trace administrator has setup your new account with this temporary PIN: " + user.pin + "."
    body = body + " Please login to http://guardiantrace.net/logout using your email and temporary PIN to select a new PIN."


    Net::SMTP.start(
        'smtp.1and1.com',
        25,
        'guardiantrace.com',
        'alert@guardiantrace.com',
        'Pioneer1',
        :plain)  do |smtp|

      smtp.open_message_stream('Alert@GuardianTrace.com', addresses) do |f|
        f.puts 'From: Admin@GuardianTrace.com'
        f.puts 'To: ' + addresses.map {|e| ["; ", e]}.flatten(1).drop(1).join
        f.puts 'Subject: ' + subject
        f.puts ''
        f.puts body
      end
    end
  end
end