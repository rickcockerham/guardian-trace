class Organization < ActiveRecord::Base
  attr_accessible :org_name, :address1, :address2, :city, :state, :postal_code, :country, :logo_image,
                  :setting_id, :active, :status_image, :users_attributes, :setting_attributes

  validates_presence_of :org_name, :on => :create
  validates_uniqueness_of :org_name

  has_many :users, :dependent => :destroy
  has_many :outstandings
  has_one :setting, :dependent => :destroy
  accepts_nested_attributes_for :users
  accepts_nested_attributes_for :setting

  def set_status_image
    if self.active?
      self.status_image = '/assets/images/stoplight-green.jpg'
    else
       self.status_image = '/assets/images/stoplight-red.jpg'
    end
  end

  def deactivate_account
    self.update_attributes(:active => false)
  end

  def save_admin_user_to_settings
    unless self.users.first.nil?
      self.setting.update_attributes(:user_id => self.users.first.id)
      self.update_attributes(:setting_id => self.setting.id)
    else
      @user = self.users.create(email: 'admin@gt.com', password: '1234', password_confirmation: '1234', admin: 1, super_user: 0)
      self.setting.update_attributes(:user_id => @user.id)
    end
  end
end
