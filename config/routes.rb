Auth::Application.routes.draw do
  resources :global_settings
  resources :sessions do
    member do
      get 'new_admin'
      #get 'admin_destroy'
      #get 'create_admin'
    end
  end
  resources :password_resets

  resources :organizations do
    collection do
      get 'index_active_orgs'
    end
    member do
      get 'deactivate_account'
    end
    resources :settings
    resources :settings
    resources :users do
      member do
        get 'admin_login'
        get 'new_admin'
        get 'set_pin'
        get 'save_pin'
      end
    end
    resources :outstandings do
      collection do
        get 'expired_index'
      end
      member do
        get 'cancel_alert'
        get 'panic_alert'
        get 'sent_alert'
        get 'pin_cancel'
      end
      resources :checkins
    end
  end

  match "/delayed_job" => DelayedJobWeb, :anchor => false

  get "org_index" => "organizations#index_active_orgs", :as => "org_index"
  #get "deactivate_account" => "organizations#deactivate_account", :as => "deactivate_account"
  #resources :organizations, :member => {:organizations => :index_active_orgs}
  #resources :organizations, :member => {:organizations => :deactivate_account}

  get "pin_cancel" => "organizations#outstandings#pin_cancel", :as => "pin_cancel"
  get "cancel_alert" => "organizations#outstandings#cancel_alert", :as => "cancel_alert"
  get "panic_alert" => "organizations#outstandings#panic_alert", :as => "panic_alert"
  get "sent_alert" => "organizations#outstandings#sent_alert", :as => "sent_alert"
  get "expired" => "organizations#outstandings#expired_index", :as => "expired"
  get "active" => "organizations#outstandings#index", :as => "active"
  get "new_alert" => "organizations#outstandings#new", :as => "new_alert"
  #resources :outstandings, :member => {:outstandings => :expired_index}
  #resources :outstandings, :member => {:outstandings => :cancel_alert}
  #resources :outstandings, :member => {:outstandings => :pin_cancel}
  #resources :outstandings, :member => {:outstandings => :panic_alert}
  #resources :outstandings, :member => {:outstandings => :sent_alert}
  #resources :outstandings, :member => {:outstandings => :index}


  get "admin_login" => "sessions#create", :as => "admin_login"
  get "admin" => "sessions#new_admin", :as => "admin"
  get "admin_logout" => "sessions#destroy", :as => "admin_logout"
  #get "admin_login" => "sessions#create_admin", :as => "admin_login"
  #get "admin" => "sessions#new_admin", :as => "admin"
  #get "admin_logout" => "sessions#admin_destroy", :as => "admin_logout"
  get "logout" => "sessions#destroy", :as => "logout"
  get "login" => "sessions#new", :as => "login"
  get "signup" => "organizations#users#new", :as => "signup"
  get "set_pin" => "organizations#users#set_pin", :as => "set_pin"
  get "save_pin" => "organizations#users#save_pin", :as => "save_pin"
  #resources :users, :member => {:users => :set_pin}
  #resources :users, :member => {:users => :save_pin}
  #resources :sessions, :member => {:sessions => :new_admin}
  #resources :sessions, :member => {:sessions => :admin_destroy}
  #resources :sessions, :member => {:sessions => :create_admin}

  #if mobile_user?
    root :to => "sessions#new"
  #else
  #  root :to => "sessinons#new_admin"
  #end




end
