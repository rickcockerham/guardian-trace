require 'test_helper'

class OutstandingsControllerTest < ActionController::TestCase
  setup do
    @outstanding = outstandings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:outstandings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create outstanding" do
    assert_difference('Outstanding.count') do
      post :create, outstanding: @outstanding.attributes
    end

    assert_redirected_to outstanding_path(assigns(:outstanding))
  end

  test "should show outstanding" do
    get :show, id: @outstanding
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @outstanding
    assert_response :success
  end

  test "should update outstanding" do
    put :update, id: @outstanding, outstanding: @outstanding.attributes
    assert_redirected_to outstanding_path(assigns(:outstanding))
  end

  test "should destroy outstanding" do
    assert_difference('Outstanding.count', -1) do
      delete :destroy, id: @outstanding
    end

    assert_redirected_to outstandings_path
  end
end
