class SetupPrimaryUserForOrgsSettings < ActiveRecord::Migration
  def change
    remove_column :organizations, :primary_email
    remove_column :organizations, :user_id
    add_column :settings, :admin_user_id, :integer
  end


end
