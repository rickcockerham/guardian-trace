class CreateOutstandings < ActiveRecord::Migration
  def change
    create_table :outstandings do |t|
      t.datetime :panic_deadline
      t.text :panic_emails
      t.datetime :panic_send_time
      t.boolean :active
      t.integer :user_id

      t.timestamps
    end
  end
end
