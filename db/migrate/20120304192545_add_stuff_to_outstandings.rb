class AddStuffToOutstandings < ActiveRecord::Migration
  def change
    add_column :outstandings, :latitude, :float

    add_column :outstandings, :longitude, :float

    add_column :outstandings, :hours, :integer

    add_column :outstandings, :mins, :integer

  end
end
