class AddSendPwSetupEmail < ActiveRecord::Migration
  def change
    add_column :users, :send_pw_setup_email, :boolean, :default => true
  end
end
