class CreateGlobalSettings < ActiveRecord::Migration
  def change
    create_table :global_settings do |t|
      t.string "theme_color"
      t.string "theme_contrast_color"
      t.string "brand_image"

      t.timestamps
    end
  end
end
