class CreateOutstToJobRelations < ActiveRecord::Migration
  def change
    create_table :outst_to_job_relations do |t|
      t.integer :outstanding_id
      t.integer :delayed_job_id
      t.timestamps
    end
  end
end
