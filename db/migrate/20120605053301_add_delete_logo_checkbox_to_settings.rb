class AddDeleteLogoCheckboxToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :delete_logo, :boolean
  end
end
