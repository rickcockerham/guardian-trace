class CreateOrganizationLevel < ActiveRecord::Migration
  def change
    create_table :organization do |t|
      t.string :organization_name
      t.string :organization_image
      t.boolean :active
      t.timestamps
    end
  end
end
