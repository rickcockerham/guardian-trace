class ChangeHoursForOutstandings < ActiveRecord::Migration
  def change
    remove_column :outstandings, :hours
    remove_column :outstandings, :mins
    add_column :outstandings, :deadline_hours, :integer
    add_column :outstandings, :deadline_mins, :integer
  end

end
