class AddPanicDeadlineToCheckins < ActiveRecord::Migration
  def change
    add_column :checkins, :panic_deadline, :datetime
  end
end
