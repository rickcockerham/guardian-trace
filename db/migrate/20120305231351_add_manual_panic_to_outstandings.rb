class AddManualPanicToOutstandings < ActiveRecord::Migration
  def change
    add_column :outstandings, :manual_panic, :boolean

  end
end
