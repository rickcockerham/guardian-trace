class AddTimezoneToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :time_zone, :string
  end
end
