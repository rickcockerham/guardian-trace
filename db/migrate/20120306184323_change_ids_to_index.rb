class ChangeIdsToIndex < ActiveRecord::Migration
  def change
    remove_column :checkins, :outstanding_id

  end

  add_index :checkins, :outstanding_id


end
