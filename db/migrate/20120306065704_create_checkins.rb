class CreateCheckins < ActiveRecord::Migration
  def change
    create_table :checkins do |t|
      t.float :latitude
      t.float :longitude
      t.datetime :checkin_time
      t.datetime :updated_deadline
      t.integer :outstanding_id
      t.integer :checkin_hours
      t.integer :checkin_mins

      t.timestamps
    end
  end
end
