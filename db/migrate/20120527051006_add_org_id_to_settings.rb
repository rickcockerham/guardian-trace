class AddOrgIdToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :organization_id, :integer
  end
end
