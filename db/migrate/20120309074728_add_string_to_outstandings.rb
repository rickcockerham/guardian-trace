class AddStringToOutstandings < ActiveRecord::Migration
  def change
    add_column :outstandings, :scroller_capture, :string
  end
end
