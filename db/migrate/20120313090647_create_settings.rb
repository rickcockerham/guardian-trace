class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.integer :green_hours
      t.integer :green_mins
      t.integer :yellow_hours
      t.integer :yellow_mins
      t.integer :red_hours
      t.integer :red_mins
      t.boolean :reminder_sms
      t.integer :reminder_sms_hours
      t.integer :reminder_sms_mins
      t.string :logo_url
      t.string :comp_name
      t.string :alert_emails
      t.string :alert_smss
      t.integer :history_hours

      t.timestamps
    end
  end
end
