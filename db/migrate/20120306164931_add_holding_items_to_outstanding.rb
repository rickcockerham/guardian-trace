class AddHoldingItemsToOutstanding < ActiveRecord::Migration
  def change
    add_column :outstandings, :latitude_holding, :float
    add_column :outstandings, :longitude_holding, :float
    add_column :outstandings, :panic_deadline_holding, :datetime
    add_column :outstandings, :plus_hours, :integer
    add_column :outstandings, :plus_minutes, :integer
  end
end
