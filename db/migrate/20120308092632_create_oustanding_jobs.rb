class CreateOustandingJobs < ActiveRecord::Migration
  def change
    create_table :oustanding_jobs do |t|
      t.integer :outstanding_id
      t.datetime :deadline

      t.timestamps
    end
  end
end
