class AddStuffToUsers < ActiveRecord::Migration
  def up
    add_column :users, :name_first, :string
    add_column :users, :name_last, :string
    add_column :users, :admin, :boolean
    add_column :users, :notes, :text
    add_column :users, :current_outstanding, :integer
  end

  def down
    remove_column :users, :name_first
    remove_column :users, :name_last
    remove_column :users, :admin
    remove_column :users, :notes
    remove_column :users, :current_outstanding
  end
end
