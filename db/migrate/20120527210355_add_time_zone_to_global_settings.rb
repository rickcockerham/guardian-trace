class AddTimeZoneToGlobalSettings < ActiveRecord::Migration
  def change
    add_column :global_settings, :time_zone, :string
  end
end
