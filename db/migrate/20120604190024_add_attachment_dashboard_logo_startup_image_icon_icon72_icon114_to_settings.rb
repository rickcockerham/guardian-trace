class AddAttachmentDashboardLogoStartupImageIconIcon72Icon114ToSettings < ActiveRecord::Migration
  def self.up
    add_column :settings, :dashboard_logo_file_name, :string
    add_column :settings, :dashboard_logo_content_type, :string
    add_column :settings, :dashboard_logo_file_size, :integer
    add_column :settings, :dashboard_logo_updated_at, :datetime
    add_column :settings, :startup_image_file_name, :string
    add_column :settings, :startup_image_content_type, :string
    add_column :settings, :startup_image_file_size, :integer
    add_column :settings, :startup_image_updated_at, :datetime
    add_column :settings, :icon_file_name, :string
    add_column :settings, :icon_content_type, :string
    add_column :settings, :icon_file_size, :integer
    add_column :settings, :icon_updated_at, :datetime
  end

  def self.down
    remove_column :settings, :dashboard_logo_file_name
    remove_column :settings, :dashboard_logo_content_type
    remove_column :settings, :dashboard_logo_file_size
    remove_column :settings, :dashboard_logo_updated_at
    remove_column :settings, :startup_image_file_name
    remove_column :settings, :startup_image_content_type
    remove_column :settings, :startup_image_file_size
    remove_column :settings, :startup_image_updated_at
    remove_column :settings, :icon_file_name
    remove_column :settings, :icon_content_type
    remove_column :settings, :icon_file_size
    remove_column :settings, :icon_updated_at
  end
end
