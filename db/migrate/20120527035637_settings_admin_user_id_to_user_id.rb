class SettingsAdminUserIdToUserId < ActiveRecord::Migration
  def change
    remove_column :settings, :admin_user_id
    add_column :settings, :user_id, :integer
  end
end
