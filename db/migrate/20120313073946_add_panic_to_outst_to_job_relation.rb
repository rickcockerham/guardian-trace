class AddPanicToOutstToJobRelation < ActiveRecord::Migration
  def change
    add_column :outst_to_job_relations, :panic, :boolean
  end
end
