class CreateUserToOutstAsscs < ActiveRecord::Migration
  def change
    create_table :user_to_outst_asscs do |t|
      t.integer :outstanding_id
      t.integer :user_id
      t.boolean :active
      t.timestamps
    end
  end
end
