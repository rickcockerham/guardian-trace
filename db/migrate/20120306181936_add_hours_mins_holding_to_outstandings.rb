class AddHoursMinsHoldingToOutstandings < ActiveRecord::Migration
  def change
    add_column :outstandings, :deadline_hours_holding, :integer
    add_column :outstandings, :deadline_mins_holding, :integer
  end
end
