class AddOrgIdToOutstandings < ActiveRecord::Migration
  def change
    add_column :outstandings, :organization_id, :integer
  end
end
