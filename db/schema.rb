# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120605154236) do

  create_table "checkins", :force => true do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "checkin_time"
    t.datetime "updated_deadline"
    t.integer  "checkin_hours"
    t.integer  "checkin_mins"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.datetime "panic_deadline"
    t.integer  "outstanding_id"
    t.boolean  "first"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "global_settings", :force => true do |t|
    t.string   "theme_color"
    t.string   "theme_contrast_color"
    t.string   "brand_image"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "time_zone"
  end

  create_table "organizations", :force => true do |t|
    t.string   "org_name"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "country"
    t.string   "logo_image"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.integer  "setting_id"
    t.boolean  "active",       :default => true
    t.string   "status_image"
  end

  create_table "oustanding_jobs", :force => true do |t|
    t.integer  "outstanding_id"
    t.datetime "deadline"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "outst_to_job_relations", :force => true do |t|
    t.integer  "outstanding_id"
    t.integer  "delayed_job_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.boolean  "panic"
  end

  create_table "outstandings", :force => true do |t|
    t.datetime "panic_deadline"
    t.text     "panic_emails"
    t.datetime "panic_send_time"
    t.boolean  "active"
    t.integer  "user_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "deadline_hours"
    t.integer  "deadline_mins"
    t.boolean  "manual_panic"
    t.float    "latitude_holding"
    t.float    "longitude_holding"
    t.datetime "panic_deadline_holding"
    t.integer  "plus_hours"
    t.integer  "plus_minutes"
    t.integer  "deadline_hours_holding"
    t.integer  "deadline_mins_holding"
    t.string   "scroller_capture"
    t.string   "status"
    t.integer  "organization_id"
  end

  create_table "settings", :force => true do |t|
    t.integer  "green_hours"
    t.integer  "green_mins"
    t.integer  "yellow_hours"
    t.integer  "yellow_mins"
    t.integer  "red_hours"
    t.integer  "red_mins"
    t.boolean  "reminder_sms"
    t.integer  "reminder_sms_hours"
    t.integer  "reminder_sms_mins"
    t.string   "logo_url"
    t.string   "comp_name"
    t.string   "alert_emails"
    t.string   "alert_smss"
    t.integer  "history_hours"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.string   "time_zone"
    t.integer  "user_id"
    t.integer  "organization_id"
    t.string   "dashboard_logo_file_name"
    t.string   "dashboard_logo_content_type"
    t.integer  "dashboard_logo_file_size"
    t.datetime "dashboard_logo_updated_at"
    t.string   "startup_image_file_name"
    t.string   "startup_image_content_type"
    t.integer  "startup_image_file_size"
    t.datetime "startup_image_updated_at"
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.boolean  "delete_logo"
  end

  create_table "user_to_outst_asscs", :force => true do |t|
    t.integer  "outstanding_id"
    t.integer  "user_id"
    t.boolean  "active"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "auth_token"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "name_first"
    t.string   "name_last"
    t.boolean  "admin"
    t.text     "notes"
    t.integer  "current_outstanding"
    t.string   "pin"
    t.boolean  "registered"
    t.string   "sms"
    t.boolean  "super_user"
    t.integer  "organization_id"
    t.boolean  "send_pw_setup_email",    :default => true
  end

end
