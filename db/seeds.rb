# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Setting.create()
User.create(email: 'admin@gt.com', password: '1234', password_confirmation: '1234', admin: 1, super_user: 1)
#Setting.create(green_hours: 99, green_mins: 59, yellow_hours: 0, yellow_mins: 15, red_hours: 0, red_mins: 0, reminder_sms: 't', reminder_sms_hours: 0, reminder_sms_mins: 15, history_hours: 24)
GlobalSettings.create(theme_color: '#ffffff', theme_contrast_color: '#000000')

